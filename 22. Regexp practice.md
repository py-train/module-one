# Regexp practice

Count the number of occurrences of the word "it" in the documentation of the built-in `open` function. The match must be case-insensitive, and must only match stand-alone words.

```python
>>> import re
>>> len(re.findall(r'\bit\b', open.__doc__, re.I))
16
```

Now build a function that receives a string and a word, and returns the count of all the occurrences of that word within the string.

```python
>>> def count_word(txt, word):
...     word_re = r'\b%s\b' % re.escape(word)
...     return len(re.findall(word_re, txt, re.I))
... 
>>> count_word(open.__doc__, "it")
16
```

