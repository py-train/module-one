# Exception handling

Consider what would happen in the example above if an error occurred between opening the file and closing it. An exception would be triggered, and the program would never have the chance to close the file, leaving potentially precious data un-flushed to disk, or leaving file descriptors open if the exception were caught elsewhere and program execution continued.

Python allows exceptions to be handled using the `try` / `except` statements. The full syntax is:


```python
try:
    # logic that could raise an Exception
except:
    # handle the exception
else:
    # optional: actions if execution was successful
finally:
    # optional: actions to take regardless of success (e.g. cleanup)
```

Multiple `except` statements can be used, for selective error handling:

```python
>>> l = ['a', 'b', 'c']
>>> d = {'a': 1, 'c': 3}
>>> try:
...     k = l[1]
...     v = d[k]
... except IndexError:
...     print("couldn't find that list index")
... except KeyError:
...     print("couldn't find that dictionary key")
... 
couldn't find that dictionary key
```

An exception instance can also be assigned to a variable for further inspection:

```python
>>> try:
...     1 / 0
... except Exception as e:
...     print("i caught a little", e)
... 
i caught a little division by zero
```

> Full docs: <https://docs.python.org/3/reference/compound_stmts.html#the-try-statement>

Remember that exceptions are regular Python classes, deriving from the root "`Exception`" class,
and the objects raised are instances of such classes. As such, you can define and `raise`
your own exceptions, as well as re-raise the current or a previously-stored exception.

> See also: <https://docs.python.org/3/library/exceptions.html>
