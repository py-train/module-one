# String formatting

## The "old" way

Using the modulo operator, a string (a "format") will have its placeholders
(its "conversion specifiers") substituted with the provided value(s):

```python
>>> "i am a %s string" % "special"
'i am a special string'
>>> "i am a %s string" % object()
'i am a <object object at 0x7faf8513b900> string'
>>> "i am %d times a %s string" % (2.5, True)
'i am 2 times a True string'
```

Note how the specifiers also perform type conversion on the provided values.

Making things a bit more complicated, you can also use conversion flags
and named mappings:

```python
>>> "i got %(amount)02d %(currency)s" % {'currency': 'cents', 'amount': 5}
'i got 05 cents'
```

> Docs: <https://docs.python.org/3/library/stdtypes.html#old-string-formatting>


## The "new" way

The new way implies using the `format()` method of strings, which uses curly brackets as placeholders:

```python
>>> "i am a {} string".format("special")
'i am a special string'
>>> "i am a {0}, {0} {1} string".format("very", "special")
'i am a very, very special string'
>>> "i am a {how} {what} string".format(how="truly", what="special")
'i am a truly special string'
```

Besides being able to reference arguments by index, this introduces a "fuller" syntax
which makes it possible to access object attributes and indices
(note the lack of quotes for dictionary access):

```python
>>> from collections import namedtuple
>>> P = namedtuple('P', ['x', 'y', 'z'])
>>> p = P(0, 5, -2)
>>> "i'm the point at ({point.x}, {point.y}, {point.z})".format(point=p)
"i'm the point at (0, 5, -2)"
>>> "i'm still the point at {0[0]:+03d}, {0[1]:+03d} and {0[2]:+03d}".format(p)
"i'm still the point at +00, +05 and -02"
>>> d = {'currency': 'eur', 'amount': 5.5}
>>> "i got {money[amount]:.2f} {money[currency]}".format(money=d)
'i got 5.50 eur'
```

> Read more about other formatting possibilities: <https://docs.python.org/3/library/string.html#formatstrings>
