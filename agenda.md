Python Training Level One: "Python Essentials"
==============================================

0. Course Summary:
------------------

### Primary Objective:

Turning the participants into self-sufficient Python programmers, able
to build an application from scratch, with a core understanding of
Python essentials that they can later build upon unassisted.

### Prerequisites:

- Participants are expected to have a certain level of technical
proficiency. Previous programming experience is highly recommended,
regardless of programming language.
- The desire to put the required effort into the process of learning a
new programming language.

### Target audience:

Developers, sysadmins, and other technically-oriented people, with
little or no experience in Python development.

### Course structure:

The course will be mostly practical, and highly interactive.
The participants are expected to collaborate, ask questions,
and devise their own exercises, with the trainer's assistance.

The subjects outlined below will be touched upon one by one, but not
necessarily in the order presented. For specific topics, the depth of
the practice will be adapted both to the participants' technical
abilities, and to their inclination to learn more about specific
subjects.

### Deliverables:

- At least one fully-functioning Python application, developed
gradually throughout the course, which the participants could further
develop after the end of the course. The application(s) won't
necessarily be fully-featured, but should have real-life utility.
- A daily summary of the subjects handled, along with relevant code snippets.
- Course documentation.

1. Running Python, Installing Packages
--------------------------------------

1. the python shell
2. `python <file.py>`. the shebang.
3. ipython
4. pip
5. virtualenv

2. Datatypes. Mutable vs. Immutable.
------------------------------------

1. numbers (`int`, `float`)
2. `bool`
3. `str`
4. `byte`
5. `list`
6. `tuple`
7. `dict`
8. `set`

3. The Language
---------------

1. language syntax: indentation and colons. comments, docstrings.
styleguide (PEP 8).
2. conditionals: `if:` / `elif:` / `else:`. `and`, `or`,
`not`. `in`.
3. looping: `for:` and `while:` (and `else:`). `continue`,
`break`.
4. `pass`
5. functions: definition, positional arguments, keyword arguments.
`return`.
6. `lambda:`
7. `try:` / `except:` / `else:` / `finally:`. exceptions,
`as`. `raise`.
8. scope resolution: `global` and `nonlocal`
9. modules and packages: `import`, `from ... import`, `import ... as`
10. slicing
11. `with`: context managers

4. OOP in Python
----------------

1. class definition.
2. inheritance. `cls.mro()`.
3. `__init__()`. class attributes vs. instance attributes.
4. `super()`
5. `@classmethod`, `@staticmethod`
6. `@property`

5. Essential Functions, Methods, Modules
----------------------------------------

1. builtins:
   - `dir`, `help`, `vars`, `type`, `print`
   - `range`
   - `all`, `any`, `next`
   - `isinstance`, `issubclass`
   - `zip`, `enumerate`
   - `filter`, `sorted`
   - `getattr`, `setattr`, `hasattr`
   - `locals`, `globals`
   - `map`
2. `str` methods. string formatting. string encoding / decoding.
3. `list` and `dict` operations
4. `open`: file operations
5. `sys`
6. `os.path`
7. `datetime`
8. regular expressions
9. builtin exceptions

6. Advanced Language Features:
------------------------------

1. comprehensions (list, set and dict). nested comprehensions.
2. conditional ("ternary") expressions
3. the `*` and `**` operators: `*args` and `**kwargs`,
and argument unpacking
4. generators
5. iterator functions, the `yield` statement. iterator classes^[§](#note){.footnote-ref}^.
6. double underscore attributes: `__doc__`, `__annotations__`,
`__name__`, `__class__`, `__module__`, `__dict__`, `__file__`
7. magic methods: `__call__`, `__str__`, `__repr__`;
operator overloading.
8. nested functions
9. decorators, class-based decorators^[§](#note){.footnote-ref}^.
10. `__new__`. `type` in depth. metaclasses^[§](#note){.footnote-ref}^.

7. Other Useful Modules, Types, Functions^[§](#note){.footnote-ref}^:
-----------------------------------------------------

1. `collections`: `OrderedDict`, `defaultdict`, `namedtuple`
2. `Decimal`
3. `json`
4. `csv`
5. `functools`: `partial` / `partialmethod`, `lru_cache`
6. `io`: `BytesIO`, `StringIO`
7. `os`: `walk`, `listdir`, `scandir`
8. `itertools`: `chain`, `cycle`, `islice`

10. Additional Subjects^[§](#note){.footnote-ref}^, One or More of:
----------------------------------------------------

- unit testing
- logging
- `multiprocessing`, `glob`, `argparse`, `subprocess`
- python db-api, SqlAlchemy or the Django ORM
- Flask or Django
- wxPython or PyQt


::: {#note .section .footnotes}

------------------------------------------------------------------------

^§^ time-permitting, and based on participant preferences.

:::
