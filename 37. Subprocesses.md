# Subprocesses

Most simple tasks are performed using the convenience function `subprocess.run`. The heavy lifting is performed by `subprocess.Popen`, but more on that later.

If you are only interested in running a command, do:

```python
>>> subprocess.run('ls')
code  course-notes.md  course-notes.pdf  files  work
CompletedProcess(args='ls', returncode=0)
```

To give the command arguments, use an iterable instead of a string:

```python
>>> subprocess.run(['ls', '--group-directories-first', '-F', 'code'])
parser/  versions/  dice.py*
CompletedProcess(args=['ls', '--group-directories-first', '-F', 'code'], returncode=0)
```

You may want to take specific actions based on the command's return status:

```python
>>> proc = subprocess.run(['ls', 'code'])
dice.py  parser  versions
>>> print(proc.returncode)
0
```

If you prefer to get an exception if the command ended with an error, use `check=True`:

```python
>>> try:
...     proc = subprocess.run(['ls', 'codez'], check=True)
... except subprocess.CalledProcessError:
...     print("that was an error")
... 
ls: cannot access 'codez': No such file or directory
that was an error
```

So far all command output got printed right along, errors too, which is likely not what you want. To capture the output, use the special value `PIPE` for `stdout` and / or `stderr`:

```python
>>> proc = subprocess.run(['ls', 'code'], stdout=subprocess.PIPE)
>>> proc.stdout
b'dice.py\nparser\nversions\n'
```

`PIPE` actually applies to `stdin` as well, but when using `subprocess.run` use the `input` argument:

```python
>>> txt = b"""
... this
... is
... a
... string
... that's
... funny
... """
>>> subprocess.run(["grep", "i"], input=txt)
this
is
string
CompletedProcess(args=['grep', 'i'], returncode=0)
```

Let's see how we'd to this with the lower-level `Popen`, while capturing the output:

```python
>>> from subprocess import Popen, PIPE
>>> proc = Popen(["grep", "i"], stdin=PIPE, stdout=PIPE)
>>> proc.stdin
<_io.BufferedWriter name=16>
>>> proc.stdout
<_io.BufferedReader name=18>
>>> proc.stdin.write(txt)
31
>>> proc.stdin.close()
>>> proc.stdout.read()
b'this\nis\nstring\n'
```

The first thing we notice is that both stdin and stdout are actually file-like objects. The second thing is that we need to `.close()` stdin, or we might be stuck waiting indefinitely when trying to read from stdout.

Normally you will want to use `.communicate()` however, which takes care of possible deadlocks, and returns a tuple of stdout and stderr contents:

```python
>>> proc = Popen(["grep", "i"], stdin=PIPE, stdout=PIPE)
>>> proc.communicate(txt)
(b'this\nis\nstring\n', None)
```

But this isn't a feasible approach when dealing with long-lived processes, or processes with lots of output. In this case you need to  roll your sleeves up and go low-level:

```python
>>> p1 = Popen(['tail', '-f', '/var/log/syslog'], stdout=PIPE)
>>> p2 = Popen(['grep', '-i', 'usb'], stdin=p1.stdout)
```

Noticed how we connected one process's stdout to another's stdin? This is exactly what the pipe symbol in the shell does.

Now go ahead and connect / disconnect some usb device. We'll want to do something with that output however.

```python
>>> p2.terminate()
>>> p2 = Popen(['grep', '-i', 'usb'], stdin=p1.stdout, stdout=PIPE)
>>> from threading import Thread
>>> def readit(proc):
...     while proc.poll() is None:
...         line = proc.stdout.readline()
...         if line:
...             print("i got: " + line.decode("utf-8"))
... 
>>> t = Thread(target=readit, args=(p2.stdout,), daemon=True)
>>> t.start()

```

Yes, that was a crash course in `threading` too. We need to do the reading in a separate thread, or things deadlock quickly.

Now go ahead and connect / disconnect / connect / disconnect those usb devices. Nothing happening? Keep at it for a while.

Ok, so what's going on? The output is buffered, because `grep` decided behind the curtains that it should use the OS's default buffering settings, as its stdout isn't a terminal anymore. Thankfully, grep can be forced to output in line-buffered mode, but if you *really* need to read the output real-time, other apps need more dire solutions which fool them into thinking they're running under a terminal.

Note that python will do its own buffering as well, so make sure things are line-buffered on its side too:

```python
>>> p1.terminate()
>>> p2.terminate()
>>> t.join()
>>> def readit(proc):
...     while proc.poll() is None:
...         line = proc.stdout.readline()
...         if line:
...             print("i got: " + line)
... 
>>> p1 = Popen(['tail', '-f', '/var/log/syslog'], stdout=PIPE, universal_newlines=True)
>>> p2 = Popen(['grep', '--line-buffered', '-i', 'usb'],
...   stdin=p1.stdout, stdout=PIPE,
...   bufsize=1, universal_newlines=True)
>>> t = Thread(target=readit, args=(p2,), daemon=True)
>>> t.start()
```

Note how line-buffering (`bufsize=1`) needs universal newlines enabled, which implies working in text mode, which means we need to change the threaded function, and make sure the first subprocess works in text mode too.

(Note: starting with Python 3.7. you can use `text=True` instead of `universal_newlines=True`.)

Finally, remember that buffering is there for a reason, and switching it off will have a performance penalty.

> If you want to work with subprocesses, do read the docs: <https://docs.python.org/3/library/subprocess.html> to be aware of the entire API.

